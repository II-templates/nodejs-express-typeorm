"use strict";
/// <reference path="serve-static.d.ts" />
/// <reference path="../express/express.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
const express = require("express");
const serveStatic = require("serve-static");
var app = express();
app.use(serveStatic('/1'));
app.use(serveStatic('/2', {}));
app.use(serveStatic('/3', {
    dotfiles: 'ignore',
    etag: true,
    extensions: ['html'],
    fallthrough: true,
    index: true,
    lastModified: true,
    maxAge: 0,
    redirect: true,
    setHeaders: function (res, path, stat) {
        res.setHeader('Server', 'server-static middleware');
    }
}));
serveStatic.mime.define({
    'application/babylon': ['babylon'],
    'application/babylonmeshdata': ['babylonmeshdata'],
    'application/fx': ['fx']
});
//# sourceMappingURL=serve-static-tests.js.map