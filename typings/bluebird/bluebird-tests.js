"use strict";
///<reference path="bluebird.d.ts"/>
Object.defineProperty(exports, "__esModule", { value: true });
// Tests by: Bart van der Schoor <https://github.com/Bartvds>
// Note: replicate changes to all overloads in both definition and test file
// Note: keep both static and instance members inline (so similar)
// Note: try to maintain the ordering and separators, and keep to the pattern
const Promise = require("bluebird");
var obj;
var bool;
var num;
var str;
var err;
var x;
var f;
var func;
var arr;
var exp;
var anyArr;
var strArr;
var numArr;
var voidVar;
// - - - - - - - - - - - - - - - - -
var value;
var reason;
var insanity;
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
var foo;
var bar;
var baz;
var fooArr;
var barArr;
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
var numProm;
var strProm;
var anyProm;
var boolProm;
var objProm;
var voidProm;
var fooProm;
var barProm;
var barOrVoidProm;
var fooOrBarProm;
var bazProm;
// - - - - - - - - - - - - - - - - -
var numThen;
var strThen;
var anyThen;
var boolThen;
var objThen;
var voidThen;
var fooThen;
var barThen;
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
var numArrProm;
var strArrProm;
var anyArrProm;
var fooArrProm;
var barArrProm;
// - - - - - - - - - - - - - - - - -
var numArrThen;
var strArrThen;
var anyArrThen;
var fooArrThen;
var barArrThen;
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
var numPromArr;
var strPromArr;
var anyPromArr;
var fooPromArr;
var barPromArr;
// - - - - - - - - - - - - - - - - -
var numThenArr;
var strThenArr;
var anyThenArr;
var fooThenArr;
var barThenArr;
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// booya!
var fooThenArrThen;
var barThenArrThen;
var fooResolver;
var barResolver;
var fooInspection;
var fooInspectionPromise;
var fooInspectionArrProm;
var barInspectionArrProm;
var BlueBird;
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
var nodeCallbackFunc = (callback) => { };
var nodeCallbackFuncErrorOnly = (callback) => { };
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
fooThen = fooProm;
barThen = barProm;
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
fooProm = new Promise((resolve, reject) => {
    if (bool) {
        resolve(foo);
    }
    else {
        reject(new Error(str));
    }
});
fooProm = new Promise((resolve) => {
    if (bool) {
        resolve(foo);
    }
});
// - - - - - - - - - - - - - - - - - - - - - - -
// needs a hint when used untyped?
fooProm = new Promise((resolve, reject) => {
    if (bool) {
        resolve(fooThen);
    }
    else {
        reject(new Error(str));
    }
});
fooProm = new Promise((resolve) => {
    resolve(fooThen);
});
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
fooResolver.resolve(foo);
fooResolver.reject(err);
fooResolver.callback = (err, value) => {
};
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
bool = fooInspection.isFulfilled();
bool = fooInspection.isRejected();
bool = fooInspection.isPending();
foo = fooInspection.value();
x = fooInspection.reason();
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
barProm = fooProm.then((value) => {
    return bar;
}, (reason) => {
    return bar;
});
barProm = fooProm.then((value) => {
    return bar;
}, (reason) => {
    return barProm;
});
barOrVoidProm = fooProm.then((value) => {
    return bar;
}, (reason) => {
    return;
});
barOrVoidProm = fooProm.then((value) => {
    return bar;
}, (reason) => {
    return voidProm;
});
barProm = fooProm.then((value) => {
    return bar;
});
barProm = barProm.then((value) => {
    if (value)
        return value;
    var b;
    return Promise.resolve(b);
});
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
fooProm = fooProm.catch((reason) => {
    return;
});
fooProm = fooProm.caught((reason) => {
    return;
});
fooProm = fooProm.catch((error) => {
    return true;
}, (reason) => {
    return;
});
fooProm = fooProm.caught((error) => {
    return true;
}, (reason) => {
    return;
});
fooProm = fooProm.catch((reason) => {
    return voidProm;
});
fooProm = fooProm.caught((reason) => {
    return voidProm;
});
fooProm = fooProm.catch((error) => {
    return true;
}, (reason) => {
    return voidProm;
});
fooProm = fooProm.caught((error) => {
    return true;
}, (reason) => {
    return voidProm;
});
fooProm = fooProm.catch((reason) => {
    //handle multiple valid return types simultaneously
    if (foo === null) {
        return;
    }
    else if (!reason) {
        return voidProm;
    }
    else if (foo) {
        return foo;
    }
});
fooOrBarProm = fooProm.catch((reason) => {
    return bar;
});
fooOrBarProm = fooProm.caught((reason) => {
    return bar;
});
fooOrBarProm = fooProm.catch((error) => {
    return true;
}, (reason) => {
    return bar;
});
fooOrBarProm = fooProm.caught((error) => {
    return true;
}, (reason) => {
    return bar;
});
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
fooProm = fooProm.catch(Error, (reason) => {
    return;
});
fooProm = fooProm.catch(Promise.CancellationError, (reason) => {
    return;
});
fooProm = fooProm.caught(Error, (reason) => {
    return;
});
fooProm = fooProm.caught(Promise.CancellationError, (reason) => {
    return;
});
fooOrBarProm = fooProm.catch(Error, (reason) => {
    return bar;
});
fooOrBarProm = fooProm.catch(Promise.CancellationError, (reason) => {
    return bar;
});
fooOrBarProm = fooProm.caught(Error, (reason) => {
    return bar;
});
fooOrBarProm = fooProm.caught(Promise.CancellationError, (reason) => {
    return bar;
});
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
barProm = fooProm.error((reason) => {
    return bar;
});
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
fooProm = fooProm.finally(() => {
    // non-Thenable return is ignored
    return "foo";
});
fooProm = fooProm.finally(() => {
    return fooThen;
});
fooProm = fooProm.finally(() => {
    // non-Thenable return is ignored
});
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
fooProm = fooProm.lastly(() => {
    // non-Thenable return is ignored
    return "foo";
});
fooProm = fooProm.lastly(() => {
    return fooThen;
});
fooProm = fooProm.lastly(() => {
    // non-Thenable return is ignored
});
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
fooProm = fooProm.bind(obj);
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
voidVar = fooProm.done((value) => {
    return bar;
}, (reason) => {
    return bar;
});
voidVar = fooProm.done((value) => {
    return bar;
});
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
voidVar = fooProm.done((value) => {
    return barThen;
}, (reason) => {
    return barThen;
});
voidVar = fooProm.done((value) => {
    return barThen;
});
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
fooProm = fooProm.tap((value) => {
    // non-Thenable return is ignored
    return "foo";
});
fooProm = fooProm.tap((value) => {
    return fooThen;
});
fooProm = fooProm.tap((value) => {
    return voidThen;
});
fooProm = fooProm.tap(() => {
    // non-Thenable return is ignored
});
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
fooProm = fooProm.delay(num);
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
fooProm = fooProm.timeout(num);
fooProm = fooProm.timeout(num, str);
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
fooProm.nodeify();
fooProm = fooProm.nodeify((err) => { });
fooProm = fooProm.nodeify((err, foo) => { });
fooProm.nodeify({ spread: true });
fooProm = fooProm.nodeify((err) => { }, { spread: true });
fooProm = fooProm.nodeify((err, foo) => { }, { spread: true });
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
voidVar = fooProm.cancel();
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
bool = fooProm.isCancelled();
bool = fooProm.isFulfilled();
bool = fooProm.isRejected();
bool = fooProm.isPending();
bool = fooProm.isResolved();
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
anyProm = fooProm.call(str);
anyProm = fooProm.call(str, 1, 2, 3);
//TODO enable get() test when implemented
// barProm = fooProm.get(str);
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
barProm = fooProm.return(bar);
barProm = fooProm.thenReturn(bar);
voidProm = fooProm.return();
voidProm = fooProm.thenReturn();
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// fooProm
fooProm = fooProm.throw(err);
fooProm = fooProm.thenThrow(err);
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
str = fooProm.toString();
obj = fooProm.toJSON();
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
barProm = fooArrProm.spread((one, two, twotwo) => {
    return bar;
});
// - - - - - - - - - - - - - - - - -
barProm = fooArrProm.spread((one, two, twotwo) => {
    return barThen;
});
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//TODO fix collection inference
barArrProm = fooProm.all();
objProm = fooProm.props();
fooInspectionPromise = fooProm.reflect();
barProm = fooProm.any();
barArrProm = fooProm.some(num);
barProm = fooProm.race();
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
Promise.all([fooProm, barProm]).then(result => {
    result[0].foo();
    result[1].bar();
});
Promise.all([fooProm, fooProm]).then(result => {
    result[0].foo();
    result[1].foo();
});
Promise.all([fooProm, barProm, bazProm]).then(result => {
    result[0].foo();
    result[1].bar();
    result[2].baz();
});
Promise.all([fooProm, barProm, fooProm]).then(result => {
    result[0].foo();
    result[1].bar();
    result[2].foo();
});
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//TODO fix collection inference
barArrProm = fooArrProm.map((item, index, arrayLength) => {
    return bar;
});
barArrProm = fooArrProm.map((item) => {
    return bar;
});
barArrProm = fooArrProm.map((item, index, arrayLength) => {
    return bar;
}, {
    concurrency: 1
});
barArrProm = fooArrProm.map((item) => {
    return bar;
}, {
    concurrency: 1
});
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
barArrProm = fooArrProm.mapSeries((item, index, arrayLength) => {
    return bar;
});
barArrProm = fooArrProm.mapSeries((item) => {
    return bar;
});
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
barProm = fooArrProm.reduce((memo, item, index, arrayLength) => {
    return memo;
});
barProm = fooArrProm.reduce((memo, item) => {
    return memo;
}, bar);
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
fooArrProm = fooArrProm.filter((item, index, arrayLength) => {
    return bool;
});
fooArrProm = fooArrProm.filter((item) => {
    return bool;
});
fooArrProm = fooArrProm.filter((item, index, arrayLength) => {
    return bool;
}, {
    concurrency: 1
});
fooArrProm = fooArrProm.filter((item) => {
    return bool;
}, {
    concurrency: 1
});
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
fooArrProm = fooArrProm.each((item) => bar);
fooArrProm = fooArrProm.each((item, index) => index ? bar : null);
fooArrProm = fooArrProm.each((item, index, arrayLength) => bar);
fooArrProm = fooArrProm.each((item, index, arrayLength) => barProm);
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
function getMaybePromise() {
    return foo;
}
fooProm = Promise.try(() => {
    return getMaybePromise();
});
fooProm = Promise.try(() => {
    return getMaybePromise();
});
fooProm = Promise.try(() => {
    return foo;
});
// - - - - - - - - - - - - - - - - -
fooProm = Promise.try(() => {
    return fooThen;
});
// - - - - - - - - - - - - - - - - -
fooProm = Promise.try(() => {
    if (fooProm) {
        return fooProm;
    }
    return foo;
});
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
fooProm = Promise.attempt(() => {
    return getMaybePromise();
});
fooProm = Promise.attempt(() => {
    return getMaybePromise();
});
fooProm = Promise.attempt(() => {
    return foo;
});
// - - - - - - - - - - - - - - - - -
fooProm = Promise.attempt(() => {
    if (fooProm) {
        return fooProm;
    }
    return foo;
});
// - - - - - - - - - - - - - - - - -
fooProm = Promise.attempt(() => {
    return fooThen;
});
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
func = Promise.method(function () {
});
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
fooProm = Promise.resolve(foo);
fooProm = Promise.resolve(fooThen);
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
voidProm = Promise.reject(reason);
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
fooResolver = Promise.defer();
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
fooProm = Promise.cast(foo);
fooProm = Promise.cast(fooThen);
voidProm = Promise.bind(x);
bool = Promise.is(value);
Promise.longStackTraces();
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//TODO enable delay
fooProm = Promise.delay(num, fooThen);
fooProm = Promise.delay(num, foo);
voidProm = Promise.delay(num);
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
func = Promise.promisify(f);
func = Promise.promisify(f, obj);
obj = Promise.promisifyAll(obj);
anyProm = Promise.fromNode(callback => nodeCallbackFunc(callback));
anyProm = Promise.fromNode(callback => nodeCallbackFuncErrorOnly(callback));
anyProm = Promise.fromNode(callback => nodeCallbackFunc(callback), { multiArgs: true });
anyProm = Promise.fromNode(callback => nodeCallbackFuncErrorOnly(callback), { multiArgs: true });
anyProm = Promise.fromCallback(callback => nodeCallbackFunc(callback));
anyProm = Promise.fromCallback(callback => nodeCallbackFuncErrorOnly(callback));
anyProm = Promise.fromCallback(callback => nodeCallbackFunc(callback), { multiArgs: true });
anyProm = Promise.fromCallback(callback => nodeCallbackFuncErrorOnly(callback), { multiArgs: true });
function defaultFilter(name, func) {
    return util.isIdentifier(name) &&
        name.charAt(0) !== "_" &&
        !util.isClass(func);
}
function DOMPromisifier(originalMethod) {
    // return a function
    return function promisified() {
        var args = [].slice.call(arguments);
        // Needed so that the original method can be called with the correct receiver
        var self = this;
        // which returns a promise
        return new Promise(function (resolve, reject) {
            args.push(resolve, reject);
            originalMethod.apply(self, args);
        });
    };
}
obj = Promise.promisifyAll(obj, {
    suffix: "",
    filter: defaultFilter,
    promisifier: DOMPromisifier
});
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//TODO enable generator
/*
 func = Promise.coroutine(f);

 barProm = Promise.spawn<number>(f);
 */
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
BlueBird = Promise.getNewLibraryCopy();
BlueBird = Promise.noConflict();
Promise.onPossiblyUnhandledRejection((reason) => {
});
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//TODO expand tests to overloads
fooArrProm = Promise.all(fooThenArrThen);
fooArrProm = Promise.all(fooArrProm);
fooArrProm = Promise.all(fooThenArr);
fooArrProm = Promise.all(fooArr);
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
objProm = Promise.props(objProm);
objProm = Promise.props(obj);
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//TODO expand tests to overloads
fooProm = Promise.any(fooThenArrThen);
fooProm = Promise.any(fooArrProm);
fooProm = Promise.any(fooThenArr);
fooProm = Promise.any(fooArr);
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//TODO expand tests to overloads
fooProm = Promise.race(fooThenArrThen);
fooProm = Promise.race(fooArrProm);
fooProm = Promise.race(fooThenArr);
fooProm = Promise.race(fooArr);
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//TODO expand tests to overloads
fooArrProm = Promise.some(fooThenArrThen, num);
fooArrProm = Promise.some(fooArrThen, num);
fooArrProm = Promise.some(fooThenArr, num);
fooArrProm = Promise.some(fooArr, num);
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
fooArrProm = Promise.join(foo, foo, foo);
fooArrProm = Promise.join(fooThen, fooThen, fooThen);
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// map()
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// fooThenArrThen
barArrProm = Promise.map(fooThenArrThen, (item) => {
    return bar;
});
barArrProm = Promise.map(fooThenArrThen, (item) => {
    return barThen;
});
barArrProm = Promise.map(fooThenArrThen, (item, index, arrayLength) => {
    return bar;
});
barArrProm = Promise.map(fooThenArrThen, (item, index, arrayLength) => {
    return barThen;
});
barArrProm = Promise.map(fooThenArrThen, (item) => {
    return bar;
}, {
    concurrency: 1
});
barArrProm = Promise.map(fooThenArrThen, (item) => {
    return barThen;
}, {
    concurrency: 1
});
barArrProm = Promise.map(fooThenArrThen, (item, index, arrayLength) => {
    return bar;
}, {
    concurrency: 1
});
barArrProm = Promise.map(fooThenArrThen, (item, index, arrayLength) => {
    return barThen;
}, {
    concurrency: 1
});
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// fooArrThen
barArrProm = Promise.map(fooArrThen, (item) => {
    return bar;
});
barArrProm = Promise.map(fooArrThen, (item) => {
    return barThen;
});
barArrProm = Promise.map(fooArrThen, (item, index, arrayLength) => {
    return bar;
});
barArrProm = Promise.map(fooArrThen, (item, index, arrayLength) => {
    return barThen;
});
barArrProm = Promise.map(fooArrThen, (item) => {
    return bar;
}, {
    concurrency: 1
});
barArrProm = Promise.map(fooArrThen, (item) => {
    return barThen;
}, {
    concurrency: 1
});
barArrProm = Promise.map(fooArrThen, (item, index, arrayLength) => {
    return bar;
}, {
    concurrency: 1
});
barArrProm = Promise.map(fooArrThen, (item, index, arrayLength) => {
    return barThen;
}, {
    concurrency: 1
});
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// fooThenArr
barArrProm = Promise.map(fooThenArr, (item) => {
    return bar;
});
barArrProm = Promise.map(fooThenArr, (item) => {
    return barThen;
});
barArrProm = Promise.map(fooThenArr, (item, index, arrayLength) => {
    return bar;
});
barArrProm = Promise.map(fooThenArr, (item, index, arrayLength) => {
    return barThen;
});
barArrProm = Promise.map(fooThenArr, (item) => {
    return bar;
}, {
    concurrency: 1
});
barArrProm = Promise.map(fooThenArr, (item) => {
    return barThen;
}, {
    concurrency: 1
});
barArrProm = Promise.map(fooThenArr, (item, index, arrayLength) => {
    return bar;
}, {
    concurrency: 1
});
barArrProm = Promise.map(fooThenArr, (item, index, arrayLength) => {
    return barThen;
}, {
    concurrency: 1
});
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// fooArr
barArrProm = Promise.map(fooArr, (item) => {
    return bar;
});
barArrProm = Promise.map(fooArr, (item) => {
    return barThen;
});
barArrProm = Promise.map(fooArr, (item, index, arrayLength) => {
    return bar;
});
barArrProm = Promise.map(fooArr, (item, index, arrayLength) => {
    return barThen;
});
barArrProm = Promise.map(fooArr, (item) => {
    return bar;
}, {
    concurrency: 1
});
barArrProm = Promise.map(fooArr, (item) => {
    return barThen;
}, {
    concurrency: 1
});
barArrProm = Promise.map(fooArr, (item, index, arrayLength) => {
    return bar;
}, {
    concurrency: 1
});
barArrProm = Promise.map(fooArr, (item, index, arrayLength) => {
    return barThen;
}, {
    concurrency: 1
});
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// mapSeries()
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// fooThenArrThen
barArrProm = Promise.mapSeries(fooThenArrThen, (item) => {
    return bar;
});
barArrProm = Promise.mapSeries(fooThenArrThen, (item) => {
    return barThen;
});
barArrProm = Promise.mapSeries(fooThenArrThen, (item, index, arrayLength) => {
    return bar;
});
barArrProm = Promise.mapSeries(fooThenArrThen, (item, index, arrayLength) => {
    return barThen;
});
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// fooArrThen
barArrProm = Promise.mapSeries(fooArrThen, (item) => {
    return bar;
});
barArrProm = Promise.mapSeries(fooArrThen, (item) => {
    return barThen;
});
barArrProm = Promise.mapSeries(fooArrThen, (item, index, arrayLength) => {
    return bar;
});
barArrProm = Promise.mapSeries(fooArrThen, (item, index, arrayLength) => {
    return barThen;
});
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// fooThenArr
barArrProm = Promise.mapSeries(fooThenArr, (item) => {
    return bar;
});
barArrProm = Promise.mapSeries(fooThenArr, (item) => {
    return barThen;
});
barArrProm = Promise.mapSeries(fooThenArr, (item, index, arrayLength) => {
    return bar;
});
barArrProm = Promise.mapSeries(fooThenArr, (item, index, arrayLength) => {
    return barThen;
});
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// fooArr
barArrProm = Promise.mapSeries(fooArr, (item) => {
    return bar;
});
barArrProm = Promise.mapSeries(fooArr, (item) => {
    return barThen;
});
barArrProm = Promise.mapSeries(fooArr, (item, index, arrayLength) => {
    return bar;
});
barArrProm = Promise.mapSeries(fooArr, (item, index, arrayLength) => {
    return barThen;
});
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// reduce()
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// fooThenArrThen
barProm = Promise.reduce(fooThenArrThen, (memo, item) => {
    return memo;
}, bar);
barProm = Promise.reduce(fooThenArrThen, (memo, item) => {
    return barThen;
}, bar);
barProm = Promise.reduce(fooThenArrThen, (memo, item, index, arrayLength) => {
    return memo;
}, bar);
barProm = Promise.reduce(fooThenArrThen, (memo, item, index, arrayLength) => {
    return barThen;
}, bar);
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// fooArrThen
barProm = Promise.reduce(fooArrThen, (memo, item) => {
    return memo;
}, bar);
barProm = Promise.reduce(fooArrThen, (memo, item) => {
    return barThen;
}, bar);
barProm = Promise.reduce(fooArrThen, (memo, item, index, arrayLength) => {
    return memo;
}, bar);
barProm = Promise.reduce(fooArrThen, (memo, item, index, arrayLength) => {
    return barThen;
}, bar);
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// fooThenArr
barProm = Promise.reduce(fooThenArr, (memo, item) => {
    return memo;
}, bar);
barProm = Promise.reduce(fooThenArr, (memo, item) => {
    return barThen;
}, bar);
barProm = Promise.reduce(fooThenArr, (memo, item, index, arrayLength) => {
    return memo;
}, bar);
barProm = Promise.reduce(fooThenArr, (memo, item, index, arrayLength) => {
    return barThen;
}, bar);
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// fooArr
barProm = Promise.reduce(fooArr, (memo, item) => {
    return memo;
}, bar);
barProm = Promise.reduce(fooArr, (memo, item) => {
    return barThen;
}, bar);
barProm = Promise.reduce(fooArr, (memo, item, index, arrayLength) => {
    return memo;
}, bar);
barProm = Promise.reduce(fooArr, (memo, item, index, arrayLength) => {
    return barThen;
}, bar);
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// filter()
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// fooThenArrThen
fooArrProm = Promise.filter(fooThenArrThen, (item) => {
    return bool;
});
fooArrProm = Promise.filter(fooThenArrThen, (item) => {
    return boolThen;
});
fooArrProm = Promise.filter(fooThenArrThen, (item, index, arrayLength) => {
    return bool;
});
fooArrProm = Promise.filter(fooThenArrThen, (item, index, arrayLength) => {
    return boolThen;
});
fooArrProm = Promise.filter(fooThenArrThen, (item) => {
    return bool;
}, {
    concurrency: 1
});
fooArrProm = Promise.filter(fooThenArrThen, (item) => {
    return boolThen;
}, {
    concurrency: 1
});
fooArrProm = Promise.filter(fooThenArrThen, (item, index, arrayLength) => {
    return bool;
}, {
    concurrency: 1
});
fooArrProm = Promise.filter(fooThenArrThen, (item, index, arrayLength) => {
    return boolThen;
}, {
    concurrency: 1
});
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// fooArrThen
fooArrProm = Promise.filter(fooArrThen, (item) => {
    return bool;
});
fooArrProm = Promise.filter(fooArrThen, (item) => {
    return boolThen;
});
fooArrProm = Promise.filter(fooArrThen, (item, index, arrayLength) => {
    return bool;
});
fooArrProm = Promise.filter(fooArrThen, (item, index, arrayLength) => {
    return boolThen;
});
fooArrProm = Promise.filter(fooArrThen, (item) => {
    return bool;
}, {
    concurrency: 1
});
fooArrProm = Promise.filter(fooArrThen, (item) => {
    return boolThen;
}, {
    concurrency: 1
});
fooArrProm = Promise.filter(fooArrThen, (item, index, arrayLength) => {
    return bool;
}, {
    concurrency: 1
});
fooArrProm = Promise.filter(fooArrThen, (item, index, arrayLength) => {
    return boolThen;
}, {
    concurrency: 1
});
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// fooThenArr
fooArrProm = Promise.filter(fooThenArr, (item) => {
    return bool;
});
fooArrProm = Promise.filter(fooThenArr, (item) => {
    return boolThen;
});
fooArrProm = Promise.filter(fooThenArr, (item, index, arrayLength) => {
    return bool;
});
fooArrProm = Promise.filter(fooThenArr, (item, index, arrayLength) => {
    return boolThen;
});
fooArrProm = Promise.filter(fooThenArr, (item) => {
    return bool;
}, {
    concurrency: 1
});
fooArrProm = Promise.filter(fooThenArr, (item) => {
    return boolThen;
}, {
    concurrency: 1
});
fooArrProm = Promise.filter(fooThenArr, (item, index, arrayLength) => {
    return bool;
}, {
    concurrency: 1
});
fooArrProm = Promise.filter(fooThenArr, (item, index, arrayLength) => {
    return boolThen;
}, {
    concurrency: 1
});
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// fooArr
fooArrProm = Promise.filter(fooArr, (item) => {
    return bool;
});
fooArrProm = Promise.filter(fooArr, (item) => {
    return boolThen;
});
fooArrProm = Promise.filter(fooArr, (item, index, arrayLength) => {
    return bool;
});
fooArrProm = Promise.filter(fooArr, (item, index, arrayLength) => {
    return boolThen;
});
fooArrProm = Promise.filter(fooArr, (item) => {
    return bool;
}, {
    concurrency: 1
});
fooArrProm = Promise.filter(fooArr, (item) => {
    return boolThen;
}, {
    concurrency: 1
});
fooArrProm = Promise.filter(fooArr, (item, index, arrayLength) => {
    return bool;
}, {
    concurrency: 1
});
fooArrProm = Promise.filter(fooArr, (item, index, arrayLength) => {
    return boolThen;
}, {
    concurrency: 1
});
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// each()
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// fooThenArrThen
fooArrThen = Promise.each(fooThenArrThen, (item) => bar);
fooArrThen = Promise.each(fooThenArrThen, (item) => barThen);
fooArrThen = Promise.each(fooThenArrThen, (item, index, arrayLength) => bar);
fooArrThen = Promise.each(fooThenArrThen, (item, index, arrayLength) => barThen);
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// fooArrThen
fooArrThen = Promise.each(fooArrThen, (item) => bar);
fooArrThen = Promise.each(fooArrThen, (item) => barThen);
fooArrThen = Promise.each(fooArrThen, (item, index, arrayLength) => bar);
fooArrThen = Promise.each(fooArrThen, (item, index, arrayLength) => barThen);
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// fooThenArr
fooArrThen = Promise.each(fooThenArr, (item) => bar);
fooArrThen = Promise.each(fooThenArr, (item) => barThen);
fooArrThen = Promise.each(fooThenArr, (item, index, arrayLength) => bar);
fooArrThen = Promise.each(fooThenArr, (item, index, arrayLength) => barThen);
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// fooArr
fooArrThen = Promise.each(fooArr, (item) => bar);
fooArrThen = Promise.each(fooArr, (item) => barThen);
fooArrThen = Promise.each(fooArr, (item, index, arrayLength) => bar);
fooArrThen = Promise.each(fooArr, (item, index, arrayLength) => barThen);
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//# sourceMappingURL=bluebird-tests.js.map