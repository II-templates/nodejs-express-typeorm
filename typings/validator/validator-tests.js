"use strict";
/// <reference path='./validator.d.ts' />
Object.defineProperty(exports, "__esModule", { value: true });
const validator = require("validator");
/************************************************
*                                               *
*                  IMPORT TESTS                 *
*                                               *
************************************************/
const validator_1 = require("validator");
var import_tests;
(function (import_tests) {
    let _blacklist = validator.blacklist;
    _blacklist = validator_1.blacklist;
    let _contains = validator.contains;
    _contains = validator_1.contains;
    let _equals = validator.equals;
    _equals = validator_1.equals;
    let _escape = validator.escape;
    _escape = validator_1.escape;
    let _isAfter = validator.isAfter;
    _isAfter = validator_1.isAfter;
    let _isAlpha = validator.isAlpha;
    _isAlpha = validator_1.isAlpha;
    let _isAlphanumeric = validator.isAlphanumeric;
    _isAlphanumeric = validator_1.isAlphanumeric;
    let _isAscii = validator.isAscii;
    _isAscii = validator_1.isAscii;
    let _isBase64 = validator.isBase64;
    _isBase64 = validator_1.isBase64;
    let _isBefore = validator.isBefore;
    _isBefore = validator_1.isBefore;
    let _isBoolean = validator.isBoolean;
    _isBoolean = validator_1.isBoolean;
    let _isByteLength = validator.isByteLength;
    _isByteLength = validator_1.isByteLength;
    let _isCreditCard = validator.isCreditCard;
    _isCreditCard = validator_1.isCreditCard;
    let _isCurrency = validator.isCurrency;
    _isCurrency = validator_1.isCurrency;
    let _isDataURI = validator.isDataURI;
    _isDataURI = validator_1.isDataURI;
    let _isDate = validator.isDate;
    _isDate = validator_1.isDate;
    let _isDecimal = validator.isDecimal;
    _isDecimal = validator_1.isDecimal;
    let _isDivisibleBy = validator.isDivisibleBy;
    _isDivisibleBy = validator_1.isDivisibleBy;
    let _isEmail = validator.isEmail;
    _isEmail = validator_1.isEmail;
    let _isFQDN = validator.isFQDN;
    _isFQDN = validator_1.isFQDN;
    let _isFloat = validator.isFloat;
    _isFloat = validator_1.isFloat;
    let _isFullWidth = validator.isFullWidth;
    _isFullWidth = validator_1.isFullWidth;
    let _isHalfWidth = validator.isHalfWidth;
    _isHalfWidth = validator_1.isHalfWidth;
    let _isHexColor = validator.isHexColor;
    _isHexColor = validator_1.isHexColor;
    let _isHexadecimal = validator.isHexadecimal;
    _isHexadecimal = validator_1.isHexadecimal;
    let _isIP = validator.isIP;
    _isIP = validator_1.isIP;
    let _isISBN = validator.isISBN;
    _isISBN = validator_1.isISBN;
    let _isISIN = validator.isISIN;
    _isISIN = validator_1.isISIN;
    let _isISO8601 = validator.isISO8601;
    _isISO8601 = validator_1.isISO8601;
    let _isIn = validator.isIn;
    _isIn = validator_1.isIn;
    let _isInt = validator.isInt;
    _isInt = validator_1.isInt;
    let _isJSON = validator.isJSON;
    _isJSON = validator_1.isJSON;
    let _isLength = validator.isLength;
    _isLength = validator_1.isLength;
    let _isLowercase = validator.isLowercase;
    _isLowercase = validator_1.isLowercase;
    let _isMACAddress = validator.isMACAddress;
    _isMACAddress = validator_1.isMACAddress;
    let _isMD5 = validator.isMD5;
    _isMD5 = validator_1.isMD5;
    let _isMobilePhone = validator.isMobilePhone;
    _isMobilePhone = validator_1.isMobilePhone;
    let _isMongoId = validator.isMongoId;
    _isMongoId = validator_1.isMongoId;
    let _isMultibyte = validator.isMultibyte;
    _isMultibyte = validator_1.isMultibyte;
    let _isNull = validator.isNull;
    _isNull = validator_1.isNull;
    let _isNumeric = validator.isNumeric;
    _isNumeric = validator_1.isNumeric;
    let _isSurrogatePair = validator.isSurrogatePair;
    _isSurrogatePair = validator_1.isSurrogatePair;
    let _isURL = validator.isURL;
    _isURL = validator_1.isURL;
    let _isUUID = validator.isUUID;
    _isUUID = validator_1.isUUID;
    let _isUppercase = validator.isUppercase;
    _isUppercase = validator_1.isUppercase;
    let _isVariableWidth = validator.isVariableWidth;
    _isVariableWidth = validator_1.isVariableWidth;
    let _isWhitelisted = validator.isWhitelisted;
    _isWhitelisted = validator_1.isWhitelisted;
    let _ltrim = validator.ltrim;
    _ltrim = validator_1.ltrim;
    let _matches = validator.matches;
    _matches = validator_1.matches;
    let _normalizeEmail = validator.normalizeEmail;
    _normalizeEmail = validator_1.normalizeEmail;
    let _rtrim = validator.rtrim;
    _rtrim = validator_1.rtrim;
    let _stripLow = validator.stripLow;
    _stripLow = validator_1.stripLow;
    let _toBoolean = validator.toBoolean;
    _toBoolean = validator_1.toBoolean;
    let _toDate = validator.toDate;
    _toDate = validator_1.toDate;
    let _toFloat = validator.toFloat;
    _toFloat = validator_1.toFloat;
    let _toInt = validator.toInt;
    _toInt = validator_1.toInt;
    let _trim = validator.trim;
    _trim = validator_1.trim;
    let _unescape = validator.unescape;
    _unescape = validator_1.unescape;
    let _whitelist = validator.whitelist;
    _whitelist = validator_1.whitelist;
})(import_tests || (import_tests = {}));
/************************************************
*                                               *
*                  API TESTS                    *
*                                               *
************************************************/
let any;
// **************
// * Validators *
// **************
{
    let result;
    result = validator.contains('sample', 'sample');
    result = validator.equals('sample', 'sample');
    result = validator.isAfter('sample');
    result = validator.isAfter('sample', new Date().toString());
    result = validator.isAlpha('sample');
    result = validator.isAlpha('sample', 'ar');
    result = validator.isAlpha('sample', 'ar-AE');
    result = validator.isAlpha('sample', 'ar-BH');
    result = validator.isAlpha('sample', 'ar-DZ');
    result = validator.isAlpha('sample', 'ar-EG');
    result = validator.isAlpha('sample', 'ar-IQ');
    result = validator.isAlpha('sample', 'ar-JO');
    result = validator.isAlpha('sample', 'ar-KW');
    result = validator.isAlpha('sample', 'ar-LB');
    result = validator.isAlpha('sample', 'ar-LY');
    result = validator.isAlpha('sample', 'ar-MA');
    result = validator.isAlpha('sample', 'ar-QA');
    result = validator.isAlpha('sample', 'ar-QM');
    result = validator.isAlpha('sample', 'ar-SA');
    result = validator.isAlpha('sample', 'ar-SD');
    result = validator.isAlpha('sample', 'ar-SY');
    result = validator.isAlpha('sample', 'ar-TN');
    result = validator.isAlpha('sample', 'ar-YE');
    result = validator.isAlpha('sample', 'cs-CZ');
    result = validator.isAlpha('sample', 'de-DE');
    result = validator.isAlpha('sample', 'en-AU');
    result = validator.isAlpha('sample', 'en-GB');
    result = validator.isAlpha('sample', 'en-HK');
    result = validator.isAlpha('sample', 'en-IN');
    result = validator.isAlpha('sample', 'en-NZ');
    result = validator.isAlpha('sample', 'en-US');
    result = validator.isAlpha('sample', 'en-ZA');
    result = validator.isAlpha('sample', 'en-ZM');
    result = validator.isAlpha('sample', 'es-ES');
    result = validator.isAlpha('sample', 'fr-FR');
    result = validator.isAlpha('sample', 'hu-HU');
    result = validator.isAlpha('sample', 'nl-NL');
    result = validator.isAlpha('sample', 'pl-PL');
    result = validator.isAlpha('sample', 'pt-BR');
    result = validator.isAlpha('sample', 'pt-PT');
    result = validator.isAlpha('sample', 'ru-RU');
    result = validator.isAlpha('sample', 'sr-RS');
    result = validator.isAlpha('sample', 'sr-RS@latin');
    result = validator.isAlpha('sample', 'tr-TR');
    result = validator.isAlphanumeric('sample', 'ar');
    result = validator.isAlphanumeric('sample', 'ar-AE');
    result = validator.isAlphanumeric('sample', 'ar-BH');
    result = validator.isAlphanumeric('sample', 'ar-DZ');
    result = validator.isAlphanumeric('sample', 'ar-EG');
    result = validator.isAlphanumeric('sample', 'ar-IQ');
    result = validator.isAlphanumeric('sample', 'ar-JO');
    result = validator.isAlphanumeric('sample', 'ar-KW');
    result = validator.isAlphanumeric('sample', 'ar-LB');
    result = validator.isAlphanumeric('sample', 'ar-LY');
    result = validator.isAlphanumeric('sample', 'ar-MA');
    result = validator.isAlphanumeric('sample', 'ar-QA');
    result = validator.isAlphanumeric('sample', 'ar-QM');
    result = validator.isAlphanumeric('sample', 'ar-SA');
    result = validator.isAlphanumeric('sample', 'ar-SD');
    result = validator.isAlphanumeric('sample', 'ar-SY');
    result = validator.isAlphanumeric('sample', 'ar-TN');
    result = validator.isAlphanumeric('sample', 'ar-YE');
    result = validator.isAlphanumeric('sample', 'cs-CZ');
    result = validator.isAlphanumeric('sample', 'de-DE');
    result = validator.isAlphanumeric('sample', 'en-AU');
    result = validator.isAlphanumeric('sample', 'en-GB');
    result = validator.isAlphanumeric('sample', 'en-HK');
    result = validator.isAlphanumeric('sample', 'en-IN');
    result = validator.isAlphanumeric('sample', 'en-NZ');
    result = validator.isAlphanumeric('sample', 'en-US');
    result = validator.isAlphanumeric('sample', 'en-ZA');
    result = validator.isAlphanumeric('sample', 'en-ZM');
    result = validator.isAlphanumeric('sample', 'es-ES');
    result = validator.isAlphanumeric('sample', 'fr-FR');
    result = validator.isAlphanumeric('sample', 'fr-BE');
    result = validator.isAlphanumeric('sample', 'hu-HU');
    result = validator.isAlphanumeric('sample', 'nl-BE');
    result = validator.isAlphanumeric('sample', 'nl-NL');
    result = validator.isAlphanumeric('sample', 'pl-PL');
    result = validator.isAlphanumeric('sample', 'pt-BR');
    result = validator.isAlphanumeric('sample', 'pt-PT');
    result = validator.isAlphanumeric('sample', 'ru-RU');
    result = validator.isAlphanumeric('sample', 'sr-RS');
    result = validator.isAlphanumeric('sample', 'sr-RS@latin');
    result = validator.isAlphanumeric('sample', 'tr-TR');
    result = validator.isAscii('sample');
    result = validator.isBase64('sample');
    result = validator.isBefore('sample');
    result = validator.isBefore('sample', new Date().toString());
    result = validator.isBoolean('sample');
    let isByteLengthOptions;
    result = validator.isByteLength('sample', isByteLengthOptions);
    result = validator.isByteLength('sample', 0);
    result = validator.isByteLength('sample', 0, 42);
    result = validator.isCreditCard('sample');
    let isCurrencyOptions;
    result = validator.isCurrency('sample');
    result = validator.isCurrency('sample', isCurrencyOptions);
    result = validator.isDataURI('sample');
    result = validator.isDate('sample');
    result = validator.isDecimal('sample');
    result = validator.isDivisibleBy('sample', 2);
    let isEmailOptions;
    result = validator.isEmail('sample');
    result = validator.isEmail('sample', isEmailOptions);
    let isFQDNOptions;
    result = validator.isFQDN('sample');
    result = validator.isFQDN('sample', isFQDNOptions);
    let isFloatOptions;
    result = validator.isFloat('sample');
    result = validator.isFloat('sample', isFloatOptions);
    result = validator.isFullWidth('sample');
    result = validator.isHalfWidth('sample');
    result = validator.isHexColor('sample');
    result = validator.isHexadecimal('sample');
    result = validator.isIP('sample');
    result = validator.isIP('sample', 6);
    result = validator.isISBN('sample');
    result = validator.isISBN('sample', 13);
    result = validator.isISIN('sample');
    result = validator.isISO8601('sample');
    result = validator.isIn('sample', []);
    let isIntOptions;
    result = validator.isInt('sample');
    result = validator.isInt('sample', isIntOptions);
    result = validator.isJSON('sample');
    let isLengthOptions;
    result = validator.isLength('sample', isLengthOptions);
    result = validator.isLength('sample', 3);
    result = validator.isLength('sample', 3, 5);
    result = validator.isLowercase('sample');
    result = validator.isMACAddress('sample');
    result = validator.isMD5('sample');
    result = validator.isMobilePhone('sample', 'ar-DZ');
    result = validator.isMobilePhone('sample', 'ar-SA');
    result = validator.isMobilePhone('sample', 'ar-SY');
    result = validator.isMobilePhone('sample', 'cs-CZ');
    result = validator.isMobilePhone('sample', 'de-DE');
    result = validator.isMobilePhone('sample', 'da-DK');
    result = validator.isMobilePhone('sample', 'el-GR');
    result = validator.isMobilePhone('sample', 'en-AU');
    result = validator.isMobilePhone('sample', 'en-GB');
    result = validator.isMobilePhone('sample', 'en-HK');
    result = validator.isMobilePhone('sample', 'en-IN');
    result = validator.isMobilePhone('sample', 'en-NZ');
    result = validator.isMobilePhone('sample', 'en-US');
    result = validator.isMobilePhone('sample', 'en-CA');
    result = validator.isMobilePhone('sample', 'en-ZA');
    result = validator.isMobilePhone('sample', 'en-ZM');
    result = validator.isMobilePhone('sample', 'es-ES');
    result = validator.isMobilePhone('sample', 'fi-FI');
    result = validator.isMobilePhone('sample', 'fr-FR');
    result = validator.isMobilePhone('sample', 'hu-HU');
    result = validator.isMobilePhone('sample', 'it-IT');
    result = validator.isMobilePhone('sample', 'ja-JP');
    result = validator.isMobilePhone('sample', 'ms-MY');
    result = validator.isMobilePhone('sample', 'nb-NO');
    result = validator.isMobilePhone('sample', 'nn-NO');
    result = validator.isMobilePhone('sample', 'pl-PL');
    result = validator.isMobilePhone('sample', 'pt-PT');
    result = validator.isMobilePhone('sample', 'ru-RU');
    result = validator.isMobilePhone('sample', 'sr-RS');
    result = validator.isMobilePhone('sample', 'tr-TR');
    result = validator.isMobilePhone('sample', 'vi-VN');
    result = validator.isMobilePhone('sample', 'zh-CN');
    result = validator.isMobilePhone('sample', 'zh-TW');
    result = validator.isMongoId('sample');
    result = validator.isMultibyte('sample');
    result = validator.isNull('sample');
    result = validator.isNumeric('sample');
    result = validator.isSurrogatePair('sample');
    let isURLOptions;
    result = validator.isURL('sample');
    result = validator.isURL('sample', isURLOptions);
    result = validator.isUUID('sample');
    result = validator.isUUID('sample', 5);
    result = validator.isUUID('sample', 'all');
    result = validator.isUppercase('sample');
    result = validator.isVariableWidth('sample');
    result = validator.isWhitelisted('sample', 'abc');
    result = validator.isWhitelisted('sample', ['a', 'b', 'c']);
    result = validator.matches('foobar', 'foo/i');
    result = validator.matches('foobar', 'foo', 'i');
}
// **************
// * Sanitizers *
// **************
{
    let result;
    result = validator.blacklist('sample', 'abc');
    result = validator.escape('sample');
    result = validator.unescape('sample');
    result = validator.ltrim('sample');
    result = validator.ltrim('sample', ' ');
    let normalizeEmailOptions;
    result = validator.normalizeEmail('sample');
    result = validator.normalizeEmail('sample', normalizeEmailOptions);
    result = validator.rtrim('sample');
    result = validator.rtrim('sample', ' ');
    result = validator.stripLow('sample');
    result = validator.stripLow('sample', true);
}
{
    let result;
    result = validator.toBoolean(any);
    result = validator.toBoolean(any, true);
}
{
    let result;
    result = validator.toDate(any);
}
{
    let result;
    result = validator.toFloat(any);
    result = validator.toInt(any);
    result = validator.toInt(any, 10);
}
{
    let result;
    result = validator.trim('sample');
    result = validator.trim('sample', ' ');
    result = validator.whitelist('sample', 'abc');
}
{
    let str;
    str = validator.toString([123, 456, '123', '456', true, false]);
}
{
    let ver;
    ver = validator.version;
}
// **************
// * Extensions *
// **************
validator.extend('isTest', (str, options) => !str);
//# sourceMappingURL=validator-tests.js.map