"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
require("reflect-metadata");
// CREO IL SERVER HTTP/SOCKET
// ----------------------------------------------------------------
// ----------------------------------------------------------------
const express = require("express");
//import * as compression from "compression";
let app = express();
//app.use(compression);
var http = require('http').Server(app);
// ----------------------------------------------------------------
// ----------------------------------------------------------------
// ROUTER
// ----------------------------------------------------------------
// ----------------------------------------------------------------
const users_1 = require("./app/routers/users");
app.use("/users", users_1.routerUser);
// ----------------------------------------------------------------
// ----------------------------------------------------------------
// RISORSE STATICHE
// ----------------------------------------------------------------
// ----------------------------------------------------------------
// serve a parsare i parametri del bodi per il metodo POST
const bodyParser = require("body-parser");
// per mostrare i file da browser
var serveIndex = require('serve-index');
// setto la directory con le risorse statiche
var options = {
    dotfiles: "ignore",
    etag: false,
    extensions: ["htm", "html"],
    index: false,
    maxAge: "1d",
    redirect: false,
    setHeaders: (res, path, stat) => {
        res.set("x-timestamp", Date.now().toString());
    }
};
app.use(express.static(".", options), serveIndex(".", { "icons": true }));
// uso il body parser per poter gestire i parametri nel body mandati in POST
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
// ----------------------------------------------------------------
// ----------------------------------------------------------------
// AVVIO DB E SERVER
// ----------------------------------------------------------------
// ----------------------------------------------------------------
const typeorm_1 = require("typeorm");
typeorm_1.createConnection().then(connection => {
    console.log("Connessione con db");
    // finalmente... avvio il server!	
    http.listen(3000, () => {
        console.log("Server start on port 3000!");
    });
}).catch(error => console.log(error));
// ----------------------------------------------------------------
// ----------------------------------------------------------------
//# sourceMappingURL=start.js.map