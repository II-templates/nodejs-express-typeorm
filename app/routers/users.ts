
import * as express from "express";
export let routerUser = express.Router();

import { getRepository } from "typeorm";
import { User } from "../models/User";




// prelevo tutte le entity
routerUser.get("/", (req, res) => {
	var repository = getRepository(User);

	repository.find().then ( users => {
		res.json(users);
	});
});

// // prelevo un entity tramite id
// router.get("/:id", (req, res) => {
// 	let id = req.params.id;
// 	let conn = getConnection();

// 	// // utilizzo dell' EntityManager
// 	// conn.entityManager.findOneById ( User, id ).then ( user=> {
// 	// 	res.json(user);
// 	// });

// 	// utilizzo del repository
// 	let userRepository = conn.getRepository(User);
// 	userRepository.findOneById ( id ).then ( user => {
// 		res.json(user);
// 	});
// });

// // creo nuova entity
// router.post("/", (req, res) => {
// 	let conn = getConnection();
// 	let user = new User();
//     user.firstName = req.body.name;

// 	// entity manager
//     conn.manager.save(user).then(user => {
// 		res.json(user);
// 	});
// });

// // aggiorno un entity esistente
// router.patch("/:id", (req, res) => {
// 	let conn = getConnection();

// 	// // entity manager
// 	// conn.entityManager.findOneById ( User, req.params.id ).then ( user => {
// 	// 	user.title = req.body.title;
// 	// 	conn.entityManager.persist(user);
// 	// });

// 	// repository
// 	let userRepository = conn.getRepository(User);
// 	userRepository.findOneById ( req.params.id ).then ( user => {
// 		user.firstName = req.body.name;
// 		conn.manager.save(user).then ( user => {
// 			res.json(user);
// 		});
// 	});
// });

// // cancello un entity esistente
// router.delete("/:id", (req, res) => {
// 	let conn = getConnection();

// 	// repository
// 	conn.manager.findOneById ( User, req.params.id ).then ( user => {
// 		conn.manager.remove(user).then ( user => {
// 			res.json (user);
// 		})
// 	})

// 	// // repository
// 	// let userRepository = conn.getRepository(User);
// 	// userRepository.findOneById ( req.params.id ).then ( user => {
// 	// 	userRepository.remove ( user ).then ( user => {
// 	//		res.json(user);
// 	// 		// removed!
// 	// 	})
// 	// })
	
// });
